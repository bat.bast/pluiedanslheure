package org.boiteataquets.org.weather.rain.pluiedanslheure.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.boiteataquets.org.weather.rain.pluiedanslheure.model.Forecast;
import org.boiteataquets.org.weather.rain.pluiedanslheure.model.PluieDansLHeure;
import org.boiteataquets.org.weather.rain.pluiedanslheure.model.Position;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ObjectsFactory {

    public static void createJSONSampleWithRain(String jsonFileName, int minutesStartRaining) throws IOException {
        String json = createJSONStringWithRain(minutesStartRaining);
        Path jsonFile = Paths.get(System.getProperty("user.dir"), jsonFileName);
        Files.writeString(jsonFile, json, StandardOpenOption.CREATE);
    }

    public static Position buildPositionSaintMaur() {
        Position position = new Position();
        position.setLat(48.798108);
        position.setLon(2.498403);
        position.setName("Saint-Maur-des-Fossés");
        position.setPostalCode(94100);
        return position;
    }

    public static String createJSONStringWithRain(int minutesStartRaining) throws IOException {
        Date currentDate = new Date();
        long epoch = currentDate.getTime() / 1000;
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode pdlh = mapper.createObjectNode();
        pdlh.put("updated_on", epoch);
        pdlh.put("quality", 0);
        ObjectNode position = mapper.createObjectNode();
        position.put("lat", 48.798108);
        position.put("lon", 2.498403);
        position.put("alti", 40);
        position.put("name", "Saint-Maur-des-Fossés");
        position.put("country", "FR - France");
        position.put("dept", "94");
        position.put("timezone", "Europe/Paris");
        pdlh.set("position", position);

        List<Forecast> forecast = new ArrayList<Forecast>();
        for (int i = 1; i < 10; i++) {
            Forecast f = new Forecast();
            if (i * 5 >= minutesStartRaining) { // 5' = 300''
                f.setDesc("Pluie forte");
                f.setRain(4);
            } else {
                f.setDesc("Temps sec");
                f.setRain(1);
            }
            f.setDt(epoch += 300);
            forecast.add(f);
        }
        pdlh.putPOJO("forecast", forecast);

        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(pdlh);
    }

    public static PluieDansLHeure buildPluieDansLHeureFromJSONFile(String jsonFile) throws IOException {
        return PluieDansLHeure.buildPluieDansLHeureFromJSON(Files.readString(Path.of(jsonFile)));
    }

    public static PluieDansLHeure buildPluieDansLHeureFromString(String jsonFContent) throws IOException {
        return PluieDansLHeure.buildPluieDansLHeureFromJSON(jsonFContent);
    }

    public static PluieDansLHeure buildPluieDansLHeureObject(int minutesStartRaining) throws IOException {
        return ObjectsFactory.buildPluieDansLHeureFromString(ObjectsFactory.createJSONStringWithRain(minutesStartRaining));
    }
}
