package org.boiteataquets.org.weather.rain.pluiedanslheure;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.boiteataquets.org.weather.rain.pluiedanslheure.bean.FreeUsersBean;
import org.boiteataquets.org.weather.rain.pluiedanslheure.bean.PluiesDansLHeureBean;
import org.boiteataquets.org.weather.rain.pluiedanslheure.bean.PositionsBean;
import org.boiteataquets.org.weather.rain.pluiedanslheure.model.Forecast;
import org.boiteataquets.org.weather.rain.pluiedanslheure.model.PluieDansLHeure;
import org.boiteataquets.org.weather.rain.pluiedanslheure.model.Position;
import org.boiteataquets.org.weather.rain.pluiedanslheure.service.FreeSMSService;
import org.boiteataquets.org.weather.rain.pluiedanslheure.service.PluieDansLHeureService;
import org.boiteataquets.org.weather.rain.pluiedanslheure.utils.Constants;
import org.boiteataquets.org.weather.rain.pluiedanslheure.utils.ObjectsFactory;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import java.io.IOException;
import java.util.Optional;

@SpringBootTest
class PluiedanslheureApplicationTests {

    @Autowired
    private PositionsBean positionsBean;

    @Autowired
    private FreeUsersBean freeUsersBean;

    @Autowired
    private PluiesDansLHeureBean pluiesDansLHeureBean;

    @Autowired
    private FreeSMSService freeSMSService;

    @Test
    public void contextLoads() {
    }

    @Test
    public void testPluieDansLHeureJobs() throws InterruptedException {
        freeUsersBean.getFreeUsers().forEach((id, user) -> {
            user.getPositionsSubscribed().add("saint-maur-des-fosses|94100|48.798108|2.498403");
        });
        Thread.sleep(2 * 60 * 1000); // Crontab every 5', so wait for ome minute more
        Assert.isTrue(pluiesDansLHeureBean.getPluiesDansLHeure().size() == 2, "Pluie dans l'heure Bean contains 2 items");
    }

    @Test
    void sampleWithRain() throws IOException {
        PluieDansLHeure pdlh = ObjectsFactory.buildPluieDansLHeureFromJSONFile("src/test/json/sample-with-rain.json");
        Optional<Forecast> nextRain = pdlh.getNextRain();
        Assert.isTrue(nextRain.isPresent(), "Rain is detected");
        Assert.isTrue(nextRain.get().getDt() > 0, "Rain occurred in the next minutes");
    }

    @Test
    void sampleWithoutRain() throws IOException {
        PluieDansLHeure pdlh = ObjectsFactory.buildPluieDansLHeureFromJSONFile("src/test/json/sample-without-rain.json");
        Optional<Forecast> nextRain = pdlh.getNextRain();
        Assert.isTrue(nextRain.isEmpty(), "No rain");
    }

    @Test
    void sampleWithRainInLessThan5Minutes() throws IOException {
        String jsonFileName = "src/test/json/sample-with-rain-current-time.json";
        ObjectsFactory.createJSONSampleWithRain(jsonFileName, 5);
        PluieDansLHeure pdlh = ObjectsFactory.buildPluieDansLHeureFromJSONFile(jsonFileName);
        Optional<Forecast> nextRain = pdlh.getNextRainInLessThanXMinutes(6);
        Assert.isTrue(nextRain.isPresent(), "Rain in the next 6 minutes");
    }

    @Test
    void sampleWithoutRainInLessThan6Minutes() throws IOException {
        String jsonFileName = "src/test/json/sample-without-rain-current-time.json";
        ObjectsFactory.createJSONSampleWithRain(jsonFileName, 30);
        PluieDansLHeure pdlh = ObjectsFactory.buildPluieDansLHeureFromJSONFile(jsonFileName);
        Optional<Forecast> nextRain = pdlh.getNextRainInLessThanXMinutes(6);
        Assert.isTrue(nextRain.isEmpty(), "No rain in the next 6 minutes");
    }
    @Test
    public void checkSMSWithRain() throws Exception {
        freeUsersBean.getFreeUsers().forEach((id, user) -> {
            user.getPositionsSubscribed().add("Saint-Maur-des-Fossés|94100|48.798108|2.498403");
        });
        Position position = ObjectsFactory.buildPositionSaintMaur();
        PluieDansLHeure pdlh = ObjectsFactory.buildPluieDansLHeureObject(4);
        pluiesDansLHeureBean.getPluiesDansLHeure().put(position.toString(), pdlh);
        freeSMSService.notifyFreeUsers(position);
    }
}
