package org.boiteataquets.org.weather.rain.pluiedanslheure.model;

import lombok.Data;

@Data
public class Forecast {
    private long dt;
    private int rain;
    private String desc;
}
