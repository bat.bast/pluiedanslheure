package org.boiteataquets.org.weather.rain.pluiedanslheure.model;

import lombok.Data;

@Data
public class Position {
    private double lat;
    private double lon;
    private int alti;
    private String name;
    private String country;
    private int dept;
    private int postalCode;
    private String timezone;
    private static String stringSeparator = "|";

    @Override
    public String toString() {
        return getName() + stringSeparator + getPostalCode() + stringSeparator + getLat() + stringSeparator + getLon();
    }
}
