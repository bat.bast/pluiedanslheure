package org.boiteataquets.org.weather.rain.pluiedanslheure.job;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.boiteataquets.org.weather.rain.pluiedanslheure.model.Position;
import org.boiteataquets.org.weather.rain.pluiedanslheure.service.FreeSMSService;
import org.boiteataquets.org.weather.rain.pluiedanslheure.service.PluieDansLHeureService;
import org.boiteataquets.org.weather.rain.pluiedanslheure.utils.Constants;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MeteoFranceWebserviceJob extends QuartzJobBean implements Constants {

    @Autowired
    @Getter
    PluieDansLHeureService pluieDansLHeureService;

    @Autowired
    @Getter
    FreeSMSService freeSMSService;


    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        JobDataMap jobDataMap = jobExecutionContext.getMergedJobDataMap();
        Position position = (Position) jobDataMap.get("position");
        log.debug("Executing MeteoFrance Job with key {} at position {}", jobExecutionContext.getJobDetail().getKey(), position);
        try {
            getPluieDansLHeureService().requestWS(position);
            getFreeSMSService().notifyFreeUsers(position);
        } catch (Exception ex) {
            throw new JobExecutionException(ex.getMessage());
        }
    }
}
