package org.boiteataquets.org.weather.rain.pluiedanslheure;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.boiteataquets.org.weather.rain.pluiedanslheure.bean.FreeUsersBean;
import org.boiteataquets.org.weather.rain.pluiedanslheure.bean.PositionsBean;
import org.boiteataquets.org.weather.rain.pluiedanslheure.model.FreeUser;
import org.boiteataquets.org.weather.rain.pluiedanslheure.model.Position;
import org.boiteataquets.org.weather.rain.pluiedanslheure.job.MeteoFranceWebserviceJob;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@SpringBootApplication
@RestController
@Slf4j
public class PluiedanslheureApplication implements CommandLineRunner {
    // Properties from application.properties
    @Getter
    @Setter
    @Value("${free.users}")
    private List<String> users;
    @Getter
    @Setter
    @Value("${free.ids}")
    private List<String> freeIDs;
    @Getter
    @Setter
    @Value("${free.smsTokens}")
    private List<String> freeSMSTokens;
    @Getter
    @Setter
    @Value("${meteofrance.locations}")
    private List<String> locations;
    @Getter
    @Setter
    @Value("${meteofrance.webservice.job.cron}")
    private String meteoFranceWSJobCron;

    @Autowired
    @Getter
    private PositionsBean positionsBean;

    @Autowired
    @Getter
    private FreeUsersBean freeUsersBean;

    @Autowired
    @Getter
    private Scheduler meteofranceScheduler;

    private void buildFreeUsers() {
        for (int i = 0; i < getUsers().size(); i++) {
            FreeUser user = new FreeUser();
            user.setName(getUsers().get(i));
            user.setId(getFreeIDs().get(i));
            user.setSmsToken(getFreeSMSTokens().get(i));
            getFreeUsersBean().getFreeUsers().put(user.getId(), user);
            log.debug("User {} with ID {}" , user.getName(), user.getId());
        }
        log.debug("Builded " + getUsers().size() + " users");
    }

    private void buildPositions() {
        for (String location : getLocations()) {
            String[] l = location.split("\\|", -1);
            Position position = new Position();
            position.setName(l[0]);
            position.setPostalCode(Integer.parseInt(l[1]));
            position.setLat(Double.parseDouble(l[2]));
            position.setLon(Double.parseDouble(l[3]));
            getPositionsBean().getPositions().put(location, position);
            log.debug("Position {}" , position.getName());
        }
        log.debug("Builded " + getLocations().size() + " positions");
    }

    public static void main(String[] args) {
        SpringApplication.run(PluiedanslheureApplication.class, args);
    }

    @Override
    public void run(String[] args) throws IOException {
        log.info("Starting application");
        buildPositions();
        buildFreeUsers();

        buildMeteoFranceWebServiceJobs();
    }

    private void buildMeteoFranceWebServiceJobs() {
        for (String position : getPositionsBean().getPositions().keySet()) {
            try {
                JobDataMap jobDataMap = new JobDataMap();
                jobDataMap.put("position", getPositionsBean().getPositions().get(position));
                log.debug("Building WS Job for position " + position);
                JobDetail jobDetail = JobBuilder.newJob(MeteoFranceWebserviceJob.class)
                        .withIdentity(UUID.randomUUID().toString(), "meteofrance-ws-jobs")
                        .withDescription("MeteoFrance WS Job")
                        .usingJobData(jobDataMap)
                        .storeDurably()
                        .build();
                Trigger trigger = TriggerBuilder.newTrigger()
                        .forJob(jobDetail)
                        .withIdentity(jobDetail.getKey().getName(), "meteofrance-ws-triggers")
                        .withDescription("Check MeteoFrance WS")
                        .withSchedule(CronScheduleBuilder.cronSchedule(getMeteoFranceWSJobCron()))
                        .build();
                getMeteofranceScheduler().scheduleJob(jobDetail, trigger);
            } catch (SchedulerException ex) {
                log.error("Error scheduling MeteoFrance job for position " + position, ex.getMessage());
            }
        }
    }

    @GetMapping("/")
    public String hi() {
        return "Booting Spring Boot";
    }

    @GetMapping("hello")
    public String hello(@RequestParam(value = "name", defaultValue = "world") String name) {
        return String.format("Hello Spring Boot. Your name is %s", name);
    }
}
