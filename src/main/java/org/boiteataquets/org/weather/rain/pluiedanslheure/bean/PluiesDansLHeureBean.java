package org.boiteataquets.org.weather.rain.pluiedanslheure.bean;

import lombok.Getter;
import org.boiteataquets.org.weather.rain.pluiedanslheure.model.PluieDansLHeure;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Hashtable;
import java.util.Map;

@Component("pluiesDansLHeureBean")
@Scope("singleton")
public class PluiesDansLHeureBean {
    @Getter
    Hashtable<String, PluieDansLHeure> pluiesDansLHeure = new Hashtable<String, PluieDansLHeure>();
}
