package org.boiteataquets.org.weather.rain.pluiedanslheure.bean;

import lombok.Getter;
import org.boiteataquets.org.weather.rain.pluiedanslheure.model.Position;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Hashtable;
import java.util.Map;

@Component("positionsBean")
@Scope("singleton")
public class PositionsBean {
    @Getter
    Hashtable<String, Position> positions = new Hashtable<String, Position>();
}
