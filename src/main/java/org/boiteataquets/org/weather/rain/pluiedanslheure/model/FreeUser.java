package org.boiteataquets.org.weather.rain.pluiedanslheure.model;

import lombok.Data;

import java.util.Vector;

@Data
public class FreeUser {
    private String name;
    private String id;
    private String smsToken;
    private Vector<String> positionsSubscribed = new Vector(2,1) ;
}
