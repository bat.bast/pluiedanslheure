package org.boiteataquets.org.weather.rain.pluiedanslheure.service;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.boiteataquets.org.weather.rain.pluiedanslheure.bean.PluiesDansLHeureBean;
import org.boiteataquets.org.weather.rain.pluiedanslheure.model.PluieDansLHeure;
import org.boiteataquets.org.weather.rain.pluiedanslheure.model.Position;
import org.boiteataquets.org.weather.rain.pluiedanslheure.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class PluieDansLHeureService {
    @Autowired
    @Getter
    private PluiesDansLHeureBean pluiesDansLHeureBean;

    @Autowired
    @Getter
    private RestTemplate restTemplate;


    public void requestWS(Position position) throws Exception {
        log.debug("Executing WS for position {}", position);
        Map<String, String> params = new HashMap<>();
        params.put("token", Constants.meteoFranceToken);
        params.put("lat", "" + position.getLat());
        params.put("lon", "" + position.getLon());

        try {
            ResponseEntity<PluieDansLHeure> pdlhEntity = getRestTemplate().getForEntity(Constants.meteoFranceWebservice + "?token={token}&lat={lat}&lon={lon}", PluieDansLHeure.class, params);
            MediaType contentType = pdlhEntity.getHeaders().getContentType();
            HttpStatus statusCode = pdlhEntity.getStatusCode();
            if (statusCode == HttpStatus.OK && contentType.getType().equals(MediaType.APPLICATION_JSON.getType()) && contentType.getSubtype().equals(MediaType.APPLICATION_JSON.getSubtype())) {
                log.debug("Update PluieDansLHeure information for position {}", position);
                PluieDansLHeure pdlh = pdlhEntity.getBody();
                getPluiesDansLHeureBean().getPluiesDansLHeure().put(position.toString(), pdlh);
            } else {
                throw new Exception("Cannot create PluieDansLHeure object for position " + position + ": " + statusCode.value() + "/" + contentType);
            }
        } catch (RestClientException ex) {
            throw new Exception("Cannot create PluieDansLHeure object for position " + position + ": " + ex.getMessage());
        }
    }
}
