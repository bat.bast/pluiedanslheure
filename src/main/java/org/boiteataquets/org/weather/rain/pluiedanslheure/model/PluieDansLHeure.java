package org.boiteataquets.org.weather.rain.pluiedanslheure.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.springframework.cglib.core.CollectionUtils;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Data
public class PluieDansLHeure {
    private Position position;
    private long updated_on;
    private short quality;
    private List<Forecast> forecast;

    public static PluieDansLHeure buildPluieDansLHeureFromJSON(String jsonContent) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(jsonContent, PluieDansLHeure.class);
    }

    public Optional<Forecast> getNextRain() {
        List<Forecast> result = getForecast();
        CollectionUtils.filter(result, f -> ((Forecast) f).getRain() > 1);
        if (result.size() > 0) {
            return Optional.of(result.get(0));
        } else {
            return Optional.empty();
        }
    }

    public Optional<Forecast> getNextRainInLessThanXMinutes(int minutes) {
        List<Forecast> result = getForecast();
        long currentEpoch = new Date().getTime() / 1000;
        CollectionUtils.filter(result, f -> ((Forecast) f).getRain() > 1 && ((Forecast) f).getDt() > currentEpoch && ((Forecast) f).getDt() - currentEpoch < minutes * 60L);
        if (result.size() > 0) {
            return Optional.of(result.get(0));
        } else {
            return Optional.empty();
        }
    }
}
