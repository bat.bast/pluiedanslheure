package org.boiteataquets.org.weather.rain.pluiedanslheure.bean;

import lombok.Getter;
import org.boiteataquets.org.weather.rain.pluiedanslheure.model.FreeUser;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Hashtable;

@Component("freeUsersBean")
@Scope("singleton")
public class FreeUsersBean {
    @Getter
    Hashtable<String, FreeUser> freeUsers = new Hashtable<String, FreeUser>();
}
