package org.boiteataquets.org.weather.rain.pluiedanslheure.service;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.boiteataquets.org.weather.rain.pluiedanslheure.bean.FreeUsersBean;
import org.boiteataquets.org.weather.rain.pluiedanslheure.bean.PluiesDansLHeureBean;
import org.boiteataquets.org.weather.rain.pluiedanslheure.model.Forecast;
import org.boiteataquets.org.weather.rain.pluiedanslheure.model.FreeUser;
import org.boiteataquets.org.weather.rain.pluiedanslheure.model.PluieDansLHeure;
import org.boiteataquets.org.weather.rain.pluiedanslheure.model.Position;
import org.boiteataquets.org.weather.rain.pluiedanslheure.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
public class FreeSMSService {
    private final int rainDelay = 5; // Minutes

    @Autowired
    @Getter
    private PluiesDansLHeureBean pluiesDansLHeureBean;

    @Autowired
    @Getter
    private FreeUsersBean freeUsersBean;

    @Autowired
    @Getter
    private RestTemplate restTemplate;

    public void notifyFreeUsers(Position position) {
        getFreeUsersBean().getFreeUsers().forEach((id, user) -> {
            if (user.getPositionsSubscribed().contains(position.toString())) {
                notifyRegisteredFreeUser(user, position);
            }
        });
    }

    private void notifyRegisteredFreeUser(FreeUser user, Position position) {
        PluieDansLHeure pdlh = getPluiesDansLHeureBean().getPluiesDansLHeure().get(position.toString());
        if (pdlh != null) {
            Optional<Forecast> nextRain = pdlh.getNextRainInLessThanXMinutes(rainDelay);
            if (nextRain.isEmpty()) {
                log.debug("No rain before the next " + rainDelay + " minutes at position " + position);
            } else {
                log.debug("Notify registred Free User {} for position {}", user.getName(), position);
                Map<String, String> params = new HashMap<>();
                params.put("user", user.getId());
                params.put("pass", user.getSmsToken());
                long delay = (nextRain.get().getDt() - new Date().getTime() / 1000) / 60;
                params.put("msg", nextRain.get().getDesc() + " dans " + delay + " minutes"); // Message in french

                try {
                    ResponseEntity<String> smsEntity = getRestTemplate().getForEntity(Constants.freeMobileWebService + "?user={user}&pass={pass}&msg={msg}", String.class, params);
                    MediaType contentType = smsEntity.getHeaders().getContentType();
                    HttpStatus statusCode = smsEntity.getStatusCode();
                    if (statusCode == HttpStatus.OK) {
                        log.debug("Send SMS to user {} for position {}", user.getName(), position);
                    } else {
                        log.error("Cannot send SMS to user " + user.getName() + " for position " + position + ": " + statusCode.value() + "/" + contentType);
                    }
                } catch (RestClientException ex) {
                    log.error("Cannot send SMS to user " + user.getName() + " for position " + position + ": " + ex.getMessage());
                }
            }
        } else {
            log.warn("Not yet data fort position " + position);
        }
    }
}
