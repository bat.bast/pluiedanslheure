package org.boiteataquets.org.weather.rain.pluiedanslheure.utils;

public interface Constants {
    public static final String meteoFranceWebservice = "https://webservice.meteofrance.com/rain";
    public static final String meteoFranceWebsite = "https://meteofrance.com/previsions-meteo-france";
    // Token extracted from mobile application, cf. https://github.com/hacf-fr/meteofrance-api/blob/master/src/meteofrance_api/const.py
    public static final String meteoFranceToken = "__Wj7dVSTjV9YGu1guveLyDq0g7S7TfTjaHBTPTpO0kj8__";

    public static final String freeMobileWebService = "https://smsapi.free-mobile.fr/sendmsg";
}
